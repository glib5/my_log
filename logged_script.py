def set_logger():
    """
    set new logger for stream and file

    overrides "print"
    
    to be inserted and called in every script you wish to be logged

    file is produced when calling from cmd prompt
    """
    import logging
    from pathlib import Path
    name = Path(__file__).stem
    lvl = logging.INFO
    fmt = logging.Formatter('%(asctime)s [%(name)s] %(message)s', datefmt="%d/%m/%Y %H:%M:%S")
    # always create console handler
    sh = logging.StreamHandler()
    sh.setLevel(lvl)
    sh.setFormatter(fmt)
    # create file handler
    fh = logging.FileHandler(filename=f"{name}.log", mode="w")
    fh.setLevel(lvl)
    fh.setFormatter(fmt)
    logging.basicConfig(level=lvl, handlers=(sh, fh))
    logger = logging.getLogger(name)
    global print
    print = logger.info
    return 
    

def fun():
    print("inside the fun")


def main():
    # set here, because this is the function called
    set_logger()
    print("MAX!") # to console
    fun() # works too
    return 0

if __name__ == "__main__":
    main()

# the same should be done for all the scripts
# you wish to be logged
# NO importing this function
# if logfile is created, it will have the name of the 
# file called from the cmd prompt
# other files will be created, but will be empty
