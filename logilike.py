
import logging
from pathlib import Path
from typing import Callable


def create_logger(logfile:Path) -> Callable:
    """returns the log.info function"""
    assert isinstance(logfile, Path)
    assert logfile.suffix == ".log"
    log_fmt = "[%(asctime)s] %(message)s"
    date_fmt = "%Y-%m-%d %H:%M:%S"
    fmttr = logging.Formatter(fmt=log_fmt, datefmt=date_fmt)
    sh = logging.StreamHandler()
    sh.setFormatter(fmt=fmttr)
    fh = logging.FileHandler(filename=logfile, mode="a")
    fh.setFormatter(fmt=fmttr)
    log = logging.getLogger()
    log.setLevel(logging.INFO)
    log.addHandler(sh)
    log.addHandler(fh)
    loginfo = log.info
    return loginfo


def main():

    log = create_logger(logfile=Path(__file__).with_name("asdf.log"))
    log("some info!")

    return 0


if __name__ == "__main__":
    main()


